<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       // 'css/site.css',
        
        //<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">		
       //'https://fonts.googleapis.com/css?family=Raleway:400,600',
       //'https://fonts.googleapis.com/css?family=Roboto:400,700'	,
        "public/css/bootstrap.min.css",
        "public/css/font-awesome.min.css",
        "public/css/owl.carousel.css",
        "public/css/owl.theme.css",
        "public/css/owl.transitions.css",
		"public/css/nivo-slider.css",
        "public/css/meanmenu.min.css",
        "public/css/jquery-ui.css",
        "public/css/animate.css",
        "public/css/main.css",
        "public/css/style.css",
        "public/css/responsive.css",
    ];
    public $js = [
          //"public/js/jquery-1.11.3.min.js",
        "public/js/bootstrap.min.js",
		"public/js/wow.min.js",
        "public/js/jquery.meanmenu.js",
        "public/js/owl.carousel.min.js",
        "public/js/jquery.scrollUp.min.js",
        "public/js/countdon.min.js",
        "public/js/jquery-price-slider.js",
        "public/js/jquery.nivo.slider.js",
        "public/js/plugins.js",
        "public/js/main.js",
        ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
