<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-content">
			<div class="container">
	            <div class="row">
					<div class="col-md-12">
						<ul class="page-menu">
							<li><a href="index.html">Home</a></li>
							<li><a href="cart.html">Shopping Cart</a></li>
							<li class="active"><a href="checkout.html">Checkout</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<!-- CATEGORY-MENU-LIST START -->
	                    <div class="left-category-menu hidden-sm hidden-xs">
	                        <div class="left-product-cat">
	                            <div class="category-heading">
	                                <h2>categories</h2>
	                            </div>
	                            <div class="category-menu-list">
	                                <ul>
	                                    <!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">Cameras & Photography</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Handbags</a>
	                                                <ul>
	                                                    <li><a href="#">Blouses And Shirts</a></li>
	                                                    <li><a href="#">Clutches</a></li>
	                                                    <li><a href="#">Cross Body</a></li>
	                                                    <li><a href="#">Satchels</a></li>
	                                                    <li><a href="#">Sholuder</a></li>
	                                                    <li><a href="#">Toter</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Tops</a>
	                                                <ul>
	                                                    <li><a href="#">Evening</a></li>
	                                                    <li><a href="#">Long Sleeved</a></li>
	                                                    <li><a href="#">Short Sleeved</a></li>
	                                                    <li><a href="#">Sleeveless</a></li>
	                                                    <li><a href="#">T-Shirts</a></li>
	                                                    <li><a href="#">Tanks And Camis</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Dresses</a>
	                                                <ul>
	                                                    <li><a href="#">Belts</a></li>
	                                                    <li><a href="#">Cocktail</a></li>
	                                                    <li><a href="#">Day</a></li>
	                                                    <li><a href="#">Evening</a></li>
	                                                    <li><a href="#">Sundresses</a></li>
	                                                    <li><a href="#">Sweater</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Accessories</a>
	                                                <ul>
	                                                    <li><a href="#">Bras</a></li>
	                                                    <li><a href="#">Hair Accessories</a></li>
	                                                    <li><a href="#">Hats And Gloves</a></li>
	                                                    <li><a href="#">Lifestyle</a></li>
	                                                    <li><a href="#">Scarves</a></li>
	                                                    <li><a href="#">Small Leathers</a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
										<!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">Tv & Audio</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">BAGS</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">CLOTHING</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">LINGERIE</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Shoes</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">SmartPhones & Tablets</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu cat-left-drop-menu-photo-contain">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Women</a>
	                                                <ul>
	                                                    <li><a href="#">Belts</a></li>
	                                                    <li><a href="#">Jewelry</a></li>
	                                                    <li><a href="#">Socks</a></li>
	                                                    <li><a href="#">Sunglasses</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">CLOTHING</a>
	                                                <ul>
	                                                    <li><a href="#">Boots</a></li>
	                                                    <li><a href="#">Brands We Love</a></li>
	                                                    <li><a href="#">Casuals</a></li>
	                                                    <li><a href="#">Gifts And Tech</a></li>
	                                                    <li><a href="#">Gifts And Tech</a></li>
	                                                    <li><a href="#">Slippers</a></li>
	                                                    <li><a href="#">Speakers</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left cat-left-drop-menu-photo">
	                                            	<a href="#"><img src="public/img/megamenu/vmega_blog1.jpg" alt="Product"></a>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li>
	                                        <a href="#">Laptop & Computer</a>
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Sony</a></li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Mobile</a></li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Sports</a></li>
	                                    <!-- Single menu end -->

	                                    <!-- MENU ACCORDION START -->
	                                    <li class=" rx-child">
	                                        <a href="#">Books</a>
	                                    </li>
	                                    <li class=" rx-parent">
	                                        <a class="rx-default">
	                                            More categories <span class="cat-thumb  fa fa-plus"></span> 
	                                        </a>
	                                        <a class="rx-show">
	                                            close menu <span class="cat-thumb  fa fa-minus"></span>
	                                        </a>
	                                    </li>
	                                    <!-- MENU ACCORDION END -->
	                                </ul>
	                            </div>
	                        </div>
	                    </div>	
						<!-- END CATEGORY-MENU-LIST -->
						<!-- START SMALL-PRODUCT-AREA -->
						<div class="small-product-area carosel-navigation hidden-sm hidden-xs">
							<div class="row">
								<div class="col-md-12">
									<div class="area-title">
										<h3 class="title-group gfont-1">Bestseller</h3>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="active-bestseller sidebar">
									<div class="col-xs-12">
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/1.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Various Versions</a></h5>
												<div class="price-box">
													<span class="price">$99.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/2.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Established Fact</a></h5>
												<div class="price-box">
													<span class="price">$90.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/3.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Trid Palm</a></h5>
												<div class="price-box">
													<span class="price">$99.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/4.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Established Fact</a></h5>
												<div class="price-box">
													<span class="price">$90.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
									</div>
									<div class="col-xs-12">
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/5.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Various Versions</a></h5>
												<div class="price-box">
													<span class="price">$99.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/6.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Established Fact</a></h5>
												<div class="price-box">
													<span class="price">$90.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/7.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Trid Palm</a></h5>
												<div class="price-box">
													<span class="price">$99.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
										<!-- Start Single-Product -->
										<div class="single-product">
											<div class="product-img">
												<a href="#">
													<img class="primary-img" src="public/img/product/small/8.jpg" alt="Product">
												</a>
											</div>
											<div class="product-description">
												<h5><a href="#">Established Fact</a></h5>
												<div class="price-box">
													<span class="price">$90.00</span>
													<span class="old-price">$110.00</span>
												</div>
												<span class="rating">
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star"></i>
													<i class="fa fa-star-o"></i>
												</span>
											</div>
										</div>
										<!-- End Single-Product -->
									</div>
								</div>
							</div>
						</div>
						<!-- END SMALL-PRODUCT-AREA -->
					</div>
					<div class="col-md-9">
						<!-- START PRODUCT-BANNER -->
						<div class="product-banner">
							<div class="row">
								<div class="col-xs-12">
									<div class="banner">
										<a href="#"><img src="public/img/banner/12.jpg" alt="Product Banner"></a>
									</div>
								</div>
							</div>
						</div>
						<!-- END PRODUCT-BANNER -->
						<!-- Start checkout-area -->
						<div class="checkout-area">
							<div class="row">
								<div class="col-md-12">
									<div class="cart-title">
										<h2 class="entry-title">Checkout</h2>
									</div>
									<!-- Accordion start -->
									<div class="panel-group" id="accordion">
										<!-- Start 1 Checkout-options -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger" data-toggle="collapse" data-parent="#accordion" href="#checkout-options">Step 1: Checkout Options  <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="checkout-options" class="collapse in">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-6 col-xs-12">
															<div class="checkout-collapse">
																<h3 class="title-group-3 gfont-1">New Customer</h3>
																<p>Checkout Options</p>
																<div class="radio">
																	<label>
																		<input type="radio" value="register" name="account" checked/>
																		Register Account
																	</label>
																</div>
																<div class="radio">
																	<label>
																		<input type="radio" value="guest" name="account"/>
																		Guest Checkout
																	</label>
																</div>
																<p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
																<input type="submit" class="btn btn-primary" value="Continue"/>
															</div>
														</div>
														<div class="col-md-6 col-xs-12">
															<div class="checkout-collapse">
																<h3 class="title-group-3 gfont-1">Returning Customer</h3>
																<p>I am a returning customer</p>
																<div class="form-group">
																	<label>E-mail</label>
																	<input type="email" class="form-control" name="email" />
																</div>
																<div class="form-group">
																	<label>Password</label>
																	<input type="password" class="form-control" />
																	<a href="#">Forgotten Password</a>
																</div>
																<input type="submit" class="btn btn-primary" value="Login"/>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Checkout-options -->
										<!-- Start 2 Payment-Address -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger  collapsed" data-toggle="collapse" data-parent="#accordion" href="#payment-address">Step 2: Account & Billing Details <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="payment-address" class="collapse">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-6 col-xs-12">
															<fieldset id="account">
																<legend>Your Personal Details</legend>
																<div class="form-group">
																	<label><sup>*</sup>First Name</label>
																	<input type="text" class="form-control" placeholder="First Name" name="firstname" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Last Name</label>
																	<input type="text" class="form-control" placeholder="Last Name" name="lastname" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>E-mail</label>
																	<input type="email" class="form-control" placeholder="E-mail" name="email" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Telephone</label>
																	<input type="text" class="form-control" placeholder="Telephone" name="telephone" />
																</div>
																<div class="form-group">
																	<label>Fax</label>
																	<input type="text" class="form-control" placeholder="Fax" name="fax" />
																</div>
															</fieldset>
															<fieldset>
																<legend>Your Password</legend>
																<div class="form-group">
																	<label><sup>*</sup>Password</label>
																	<input type="password" class="form-control" placeholder="Password" name="password" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Password Confirm</label>
																	<input type="password" class="form-control" placeholder="Password Confirm" name="confirm" />
																</div>
															</fieldset>
														</div>
														<div class="col-md-6 col-xs-12">
															<fieldset id="address">
																<legend>Your Address</legend>
																<div class="form-group">
																	<label>Company</label>
																	<input type="text" class="form-control" placeholder="Company" name="company" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Address 1</label>
																	<input type="text" class="form-control" placeholder="Address 1" name="Address_1" />
																</div>
																<div class="form-group">
																	<label>Address 2</label>
																	<input type="text" class="form-control" placeholder="Address 2" name="Address_2" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>City</label>
																	<input type="text" class="form-control" placeholder="City" name="city" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Post Code</label>
																	<input type="text" class="form-control" placeholder="Post Code" name="postcode" />
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Country</label>
																	<select class="form-control">
																		<option> --- Please Select --- </option>
																		<option> Bangladesh </option>
																		<option> United States </option>
																		<option> United Kingdom </option>
																		<option> Canada </option>
																		<option> Malaysia </option>
																		<option> United Arab Emirates </option>
																	</select>
																</div>
																<div class="form-group">
																	<label><sup>*</sup>Region / State</label>
																	<select class="form-control">
																		<option> --- Please Select --- </option>
																		<option> Aberdeen </option>
																		<option> Bedfordshire </option>
																		<option> Caerphilly </option>
																		<option> Denbighshire </option>
																		<option> East Ayrshire </option>
																		<option> Falkirk </option>
																	</select>
																</div>
															</fieldset>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12">
															<div class="checkbox">
																<label>
																	<input type="checkbox" name="newsletter" />
																	 I wish to subscribe to the Malias1 newsletter.
																</label>
															</div>
															<div class="checkbox">
																<label>
																	<input type="checkbox" name="shipping_address" checked/>
																	 My delivery and billing addresses are the same.
																</label>
															</div>
															<div class="buttons clearfix">
																<div class="pull-right">
																	I have read and agree to the 
																	<a href="#"><b>Privacy Policy</b></a>
																	<input type="checkbox" name="agree" />
																	<input type="button" class="btn btn-primary" value="Continue" />
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End Payment-Address -->
										<!-- Start 3 shipping-Address -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#shipping-address">Step 3: Delivery Details <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="shipping-address" class="collapse">
												<div class="panel-body">
													<div class="form-horizontal">
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>First Name</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="First Name" name="firstname" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>Last Name</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="Last Name" name="lastname" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label">Company</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="Company" name="company" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>Address 1</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="Address 1" name="address_1" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label">Address 2</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="Address 2" name="address_2" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>City</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="City" name="city" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>Post Code</label>
															<div class="col-sm-10">
																<input type="text" class="form-control" placeholder="Post Code" name="postcode" />
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>Country</label>
															<div class="col-sm-10">
																<select class="form-control">
																	<option> --- Please Select --- </option>
																	<option> Bangladesh </option>
																	<option> United States </option>
																	<option> United Kingdom </option>
																	<option> Canada </option>
																	<option> Malaysia </option>
																	<option> United Arab Emirates </option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label"><sup>*</sup>Region / State</label>
															<div class="col-sm-10">
																<select class="form-control">
																	<option> --- Please Select --- </option>
																	<option> Dhaka </option>
																	<option> New York </option>
																	<option> London </option>
																	<option> Canada </option>
																	<option> Malaysia </option>
																	<option> United Arab Emirates </option>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- End shipping-Address -->
										<!-- Start 4 shipping-Method -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#shipping-method">Step 4: Delivery Method <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="shipping-method" class="collapse">
												<div class="panel-body">
													<p>Please select the preferred shipping method to use on this order.</p>
													<p><strong>Flat Rate</strong></p>
													<div class="radio">
														<label>
															<input type="radio" name="shipping_method" checked/>
															Flat Shipping Rate - $5.00
														</label>
													</div>
													<p><strong>Add Comments About Your Order</strong></p>
													<p>
														<textarea class="form-control" name="comment" rows="8"></textarea>
													</p>
													<div class="buttons pull-right">
														<input type="button" class="btn btn-primary" value="Continue" />
													</div>
												</div>
											</div>
										</div>
										<!-- End shipping-Method -->
										<!-- Start 5 Payment-Method -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#payment-method">Step 5: Payment Method  <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="payment-method" class="collapse">
												<div class="panel-body">
													<p>Please select the preferred payment method to use on this order.</p>
													<div class="radio">
														<label>
															<input type="radio" name="payment_method" checked/>
															Cash On Delivery      
														</label>
													</div>
													<p><strong>Add Comments About Your Order</strong></p>
													<p>
														<textarea class="form-control" name="comment" rows="8"></textarea>
													</p>
													<div class="buttons pull-right">
														I have read and agree to the 
														<a href="#"><b>Terms & Conditions</b></a>
														<input type="checkbox" name="agree" />
														<input type="button" class="btn btn-primary" value="Continue" />
													</div>
												</div>
											</div>
										</div>
										<!-- End Payment-Method -->
										<!-- Start 6 Checkout-Confirm -->
										<div class="panel panel_default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-trigger collapsed" data-toggle="collapse" data-parent="#accordion" href="#checkout-confirm">Step 6: Confirm Order <i class="fa fa-caret-down"></i> </a>
												</h4>
											</div>
											<div id="checkout-confirm" class="collapse">
												<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-bordered table-hover">
															<thead>
																<tr>
																	<td class="text-left">Product Name</td>
																	<td class="text-left">Model</td>
																	<td class="text-left">Quantity</td>
																	<td class="text-left">Unit Price</td>
																	<td class="text-left">Total</td>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td class="text-left">
																		<a href="#">More-Or-Less</a>
																	</td>
																	<td class="text-left">Product 14</td>
																	<td class="text-left">2</td>
																	<td class="text-left">$100.00</td>
																	<td class="text-left">$200.00</td>
																</tr>
																<tr>
																	<td class="text-left">
																		<a href="#">Aliquam Consequat</a>
																	</td>
																	<td class="text-left">Product 21</td>
																	<td class="text-left">2</td>
																	<td class="text-left">$45.00</td>
																	<td class="text-left">$90.00</td>
																</tr>
															</tbody>
															<tfoot>
																<tr>
																	<td class="text-right" colspan="4">
																		<strong>Sub-Total:</strong>
																	</td>
																	<td class="text-right">$290.00</td>
																</tr>
																<tr>
																	<td class="text-right" colspan="4">
																		<strong>Flat Shipping Rate:</strong>
																	</td>
																	<td class="text-right">$5.00</td>
																</tr>
																<tr>
																	<td class="text-right" colspan="4">
																		<strong>Flat Shipping Rate:</strong>
																	</td>
																	<td class="text-right">$5.00</td>
																</tr>
															</tfoot>
														</table>
													</div>
													<div class="buttons pull-right">
														<input type="button" class="btn btn-primary" value="Confirm Order" />
													</div>
												</div>
											</div>
										</div>
										<!-- End Checkout-Confirm -->
									</div>
									<!-- Accordion end -->
								</div>
							</div>
						</div>
						<!-- End Shopping-Cart -->
					</div>
				</div>
			</div>
			<!-- START BRAND-LOGO-AREA -->
			<div class="brand-logo-area carosel-navigation">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="area-title">
								<h3 class="title-group border-red gfont-1">Brand Logo</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="active-brand-logo">
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/4.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/5.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/6.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END BRAND-LOGO-AREA -->
</section>