<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'account';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="page-content">
			<!-- Start Account-Create-Area -->
			<div class="account-create-area">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="page-menu">
								<li><a href="index.html">Home</a></li>
								<li class="active"><a href="account.html">Account</a></li>
							</ul>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="area-title">
								<h3 class="title-group gfont-1">Create an Account</h3>
							</div>
						</div>
					</div>
					<div class="account-create">
						<!--<form action="#">-->
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
							<div class="row">
								<div class="col-md-12">
									<div class="account-create-box">
										<h2 class="box-info">Personal Information</h2>
										<div class="row">
											<div class="col-sm-4 col-xs-12">
												<div class="single-create">
													<p>First Name <sup>*</sup></p>
<!--													<input class="form-control" type="text" name="firstname"/>-->
                                                        <?= $form->field($model,'first_name')->label(false) ?>


                                                    
												</div>
											</div>
											<div class="col-sm-4 col-xs-12">
												<div class="single-create">
													<p>Last Name <sup>*</sup></p>
<!--													<input class="form-control" type="text" name="lastname"/>-->
                                                    <?= $form->field($model,'last_name')->label(false) ?>
												</div>
											</div>
											<div class="col-sm-4 col-xs-12">
												<div class="single-create">
													<p>Email Address <sup>*</sup></p>
<!--													<input class="form-control" type="email" name="email"/>-->
                                                    <?= $form->field($model,'email')->label(false) ?>
												</div>
											</div>
											<div class="col-xs-12">
												<p class="for-newsletter"><input type="checkbox" /> Sign Up for Newsletter</p>
											</div>
										</div>
									</div>
									<div class="account-create-box">
										<h2 class="box-info">Login Information</h2>
										<div class="row">
											<div class="col-md-4 col-sm-6 col-xs-12">
												<div class="single-create">
													<p>Password <sup>*</sup></p>
<!--													<input class="form-control" type="password" name="firstname"/>-->
                                                    <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
												</div>
											</div>
											<div class="col-md-4 col-sm-6 col-xs-12">
												<div class="single-create">
													<p>Confirm Password <sup>*</sup></p>
<!--													<input class="form-control" type="password" name="firstname"/>-->
                                                    <?= $form->field($model, 'password_repeat')->passwordInput()->label(false) ?>
												</div>
											</div>
										</div>
									</div>
									<div class="submit-area">
										<p class="required text-right">* Required Fields</p>
										<button type="submit" class="btn btn-primary floatright">submit</button>
                                       
										<a href="login.html" class="float-left"><span><i class="fa fa-angle-double-left"></i></span> Back</a>
									</div>
								</div>
							</div>
						<!--</form>-->
                        <?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
			<!-- End Account-Create-Area -->
			<!-- START BRAND-LOGO-AREA -->
			<div class="brand-logo-area carosel-navigation">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="area-title">
								<h3 class="title-group border-red gfont-1">Brand Logo</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="active-brand-logo">
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/4.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/5.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/6.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END BRAND-LOGO-AREA -->
</section>