<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Shop';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="page-content">
			<div class="container">
	            <div class="row">
					<div class="col-md-12">
						<ul class="page-menu">
							<li><a href="index.html">Home</a></li>
							<li class="active"><a href="#">Bestseller Product</a></li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<!-- CATEGORY-MENU-LIST START -->
	                    <div class="left-category-menu hidden-sm hidden-xs">
	                        <div class="left-product-cat">
	                            <div class="category-heading">
	                                <h2>categories</h2>
	                            </div>
	                            <div class="category-menu-list">
	                                <ul>
	                                    <!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">Cameras & Photography</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Handbags</a>
	                                                <ul>
	                                                    <li><a href="#">Blouses And Shirts</a></li>
	                                                    <li><a href="#">Clutches</a></li>
	                                                    <li><a href="#">Cross Body</a></li>
	                                                    <li><a href="#">Satchels</a></li>
	                                                    <li><a href="#">Sholuder</a></li>
	                                                    <li><a href="#">Toter</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Tops</a>
	                                                <ul>
	                                                    <li><a href="#">Evening</a></li>
	                                                    <li><a href="#">Long Sleeved</a></li>
	                                                    <li><a href="#">Short Sleeved</a></li>
	                                                    <li><a href="#">Sleeveless</a></li>
	                                                    <li><a href="#">T-Shirts</a></li>
	                                                    <li><a href="#">Tanks And Camis</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Dresses</a>
	                                                <ul>
	                                                    <li><a href="#">Belts</a></li>
	                                                    <li><a href="#">Cocktail</a></li>
	                                                    <li><a href="#">Day</a></li>
	                                                    <li><a href="#">Evening</a></li>
	                                                    <li><a href="#">Sundresses</a></li>
	                                                    <li><a href="#">Sweater</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Accessories</a>
	                                                <ul>
	                                                    <li><a href="#">Bras</a></li>
	                                                    <li><a href="#">Hair Accessories</a></li>
	                                                    <li><a href="#">Hats And Gloves</a></li>
	                                                    <li><a href="#">Lifestyle</a></li>
	                                                    <li><a href="#">Scarves</a></li>
	                                                    <li><a href="#">Small Leathers</a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
										<!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">Tv & Audio</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">BAGS</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">CLOTHING</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">LINGERIE</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Shoes</a>
	                                                <ul>
	                                                    <li><a href="#">Blazers</a></li>
	                                                    <li><a href="#">Bootees Bags</a></li>
	                                                    <li><a href="#">Jackets</a></li>
	                                                    <li><a href="#">Shoes</a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li class="arrow-plus">
	                                        <a href="#">SmartPhones & Tablets</a>
	                                        <!--  MEGA MENU START -->
	                                        <div class="cat-left-drop-menu cat-left-drop-menu-photo-contain">
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">Women</a>
	                                                <ul>
	                                                    <li><a href="#">Belts</a></li>
	                                                    <li><a href="#">Jewelry</a></li>
	                                                    <li><a href="#">Socks</a></li>
	                                                    <li><a href="#">Sunglasses</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left">
	                                                <a class="menu-item-heading" href="#">CLOTHING</a>
	                                                <ul>
	                                                    <li><a href="#">Boots</a></li>
	                                                    <li><a href="#">Brands We Love</a></li>
	                                                    <li><a href="#">Casuals</a></li>
	                                                    <li><a href="#">Gifts And Tech</a></li>
	                                                    <li><a href="#">Gifts And Tech</a></li>
	                                                    <li><a href="#">Slippers</a></li>
	                                                    <li><a href="#">Speakers</a></li>
	                                                </ul>
	                                            </div>
	                                            <div class="cat-left-drop-menu-left cat-left-drop-menu-photo">
	                                            	<a href="#"><img src="public/img/megamenu/vmega_blog1.jpg" alt="Product"></a>
	                                            </div>
	                                        </div>
	                                        <!-- MEGA MENU END -->
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li>
	                                        <a href="#">Laptop & Computer</a>
	                                    </li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Sony</a></li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Mobile</a></li>
	                                    <!-- Single menu end -->
	                                    <!-- Single menu start -->
	                                    <li><a href="#">Sports</a></li>
	                                    <!-- Single menu end -->

	                                    <!-- MENU ACCORDION START -->
	                                    <li class=" rx-child">
	                                        <a href="#">Books</a>
	                                    </li>
	                                    <li class=" rx-parent">
	                                        <a class="rx-default">
	                                            More categories <span class="cat-thumb  fa fa-plus"></span> 
	                                        </a>
	                                        <a class="rx-show">
	                                            close menu <span class="cat-thumb  fa fa-minus"></span>
	                                        </a>
	                                    </li>
	                                    <!-- MENU ACCORDION END -->
	                                </ul>
	                            </div>
	                        </div>
	                    </div>
						<!-- END CATEGORY-MENU-LIST -->
						<!-- shop-filter start -->
						<div class="shop-filter">
							<div class="area-title">
								<h3 class="title-group gfont-1">Filters Products</h3>
							</div>
							<h4 class="shop-price-title">Price</h4>
							<div class="info_widget">
								<div class="price_filter">
									<div id="slider-range"></div>
									<div class="price_slider_amount">
										<input type="text" id="amount" name="price"  placeholder="Add Your Price" />
										<input type="submit"  value="Filter"/>  
									</div>
								</div>
							</div>
						</div>
						<!-- shop-filter start -->
						<!-- filter-by start -->
						<div class="accordion_one">
							<h4><a class="accordion-trigger" data-toggle="collapse" href="#divone">Color</a></h4>
							<div id="divone" class="collapse in">
								<div class="filter-menu">
									<ul>
										<li><a href="#">Black (2)</a></li>
										<li><a href="#">Blue (2)</a></li>
										<li><a href="#">Brown (3)</a></li>
										<li><a href="#">Green (3)</a></li>
										<li><a href="#">Orange (2)</a></li>
										<li><a href="#">Pink (2)</a></li>
										<li><a href="#">Red (11)</a></li>
										<li><a href="#">Yellow (3)</a></li>
									</ul>
								</div>
							</div>
							<h4><a class="accordion-trigger" data-toggle="collapse" href="#div2">manufacture</a></h4>
							<div id="div2" class="collapse in">
								<div class="filter-menu">
									<ul>
										<li><a href="#">Chanel (2)</a></li>
										<li><a href="#">Christian Dior (2)</a></li>
										<li><a href="#">Ermenegildo (2)</a></li>
										<li><a href="#">Ferragamo (1)</a></li>
										<li><a href="#">Hermes (2)</a></li>
										<li><a href="#">Louis Vuitton (3)</a></li>
										<li><a href="#">Prada (1)</a></li>
									</ul>
								</div>
							</div>
							<h4><a class="accordion-trigger" data-toggle="collapse" href="#div3">Size</a></h4>
							<div id="div3" class="collapse in">
								<div class="filter-menu">
									<ul>
										<li><a href="#">L (1)</a></li>
										<li><a href="#">M (5)</a></li>
										<li><a href="#">S (7)</a></li>
										<li><a href="#">XL (5)</a></li>
										<li><a href="#">XXL (6)</a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- filter-by end -->
					</div>
					<div class="col-md-9 col-xs-12">
						<!-- START PRODUCT-BANNER -->
						<div class="product-banner">
							<div class="row">
								<div class="col-xs-12">
									<div class="banner">
										<a href="#"><img src="public/img/banner/12.jpg" alt="Product Banner"></a>
									</div>
								</div>
							</div>
						</div>
						<!-- END PRODUCT-BANNER -->
						<!-- START PRODUCT-AREA -->
						<div class="product-area">
							<div class="row">
								<div class="col-xs-12">
									<!-- Start Product-Menu -->
									<div class="product-menu">
										<div class="product-title">
											<h3 class="title-group-3 gfont-1">cameras & photography</h3>
										</div>
									</div>
									<div class="product-filter">
										<ul role="tablist">
											<li role="presentation" class="list">
												<a href="#display-1-1" role="tab" data-toggle="tab"></a>
											</li>
											<li role="presentation"  class="grid active">
												<a href="#display-1-2" role="tab" data-toggle="tab"></a>
											</li>
										</ul>
										<div class="sort">
											<label>Sort By:</label>
											<select>
												<option value="#">Default</option>
												<option value="#">Name (A - Z)</option>
												<option value="#">Name (Z - A)</option>
												<option value="#">Price (Low > High)</option>
												<option value="#">Rating (Highest)</option>
												<option value="#">Rating (Lowest)</option>
												<option value="#">Name (A - Z)</option>
												<option value="#">Model (Z - A))</option>
												<option value="#">Model (A - Z)</option>
											</select>
										</div>
										<div class="limit">
											<label>Show:</label>
											<select>
												<option value="#">8</option>
												<option value="#">16)</option>
												<option value="#">24</option>
												<option value="#">40</option>
												<option value="#">80</option>
												<option value="#">100</option>
											</select>
										</div>
									</div>
									
									<!-- End Product-Menu -->
									<div class="clear"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-md-12">		
									<!-- Start Product -->
									<div class="product">
										<div class="tab-content">
											<!-- Product -->
											<div role="tabpanel" class="tab-pane fade" id="display-1-1">
												<div class="row">
													<div class="listview">
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/10.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/10bg.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Various Versions</a></h5>
																	<div class="price-box">
																		<span class="price">$99.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/13.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/11.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Trid Palm</a></h5>
																	<div class="price-box">
																		<span class="price">$99.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/10bg.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/10.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Established Fact</a></h5>
																	<div class="price-box">
																		<span class="price">$92.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/3.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/3bg.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Various Versions</a></h5>
																	<div class="price-box">
																		<span class="price">$99.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/1.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/1bg.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Ultra Consequad</a></h5>
																	<div class="price-box">
																		<span class="price">$99.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/2.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/2bg.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Established Fact</a></h5>
																	<div class="price-box">
																		<span class="price">$92.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
														<!-- Start Single-Product -->
														<div class="single-product">
															<div class="col-md-3 col-sm-4 col-xs-12">
																<div class="label_new">
																	<span class="new">new</span>
																</div>
																<div class="sale-off">
																	<span class="sale-percent">-55%</span>
																</div>
																<div class="product-img">
																	<a href="#">
																		<img class="primary-img" src="public/img/product/mediam/12.jpg" alt="Product">
																		<img class="secondary-img" src="public/img/product/mediam/12bg.jpg" alt="Product">
																	</a>
																</div>
															</div>
															<div class="col-md-9 col-sm-8 col-xs-12">	
																<div class="product-description">
																	<h5><a href="#">Trid Palm</a></h5>
																	<div class="price-box">
																		<span class="price">$99.00</span>
																		<span class="old-price">$110.00</span>
																	</div>
																	<span class="rating">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	</span>
																	<p class="description">Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Viva..</p>
																	<div class="product-action">
																		<div class="button-group">
																			<div class="product-button">
																				<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																			</div>
																			<div class="product-button-2">
																				<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																				<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																				<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!-- End Single-Product -->
													</div>
												</div>
												<!-- Start Pagination Area -->
												<div class="pagination-area">
													<div class="row">
														<div class="col-xs-5">
															<div class="pagination">
																<ul>
																	<li class="active"><a href="#">1</a></li>
																	<li><a href="#">2</a></li>
																	<li><a href="#">></a></li>
																	<li><a href="#">>|</a></li>
																</ul>
															</div>
														</div>
														<div class="col-xs-7">
															<div class="product-result">
																<span>Showing 1 to 16 of 19 (2 Pages)</span>
															</div>
														</div>
													</div>
												</div>
												<!-- End Pagination Area -->
											</div>
											<!-- End Product -->
											<!-- Start Product-->
											<div role="tabpanel" class="tab-pane fade in  active" id="display-1-2">
												<div class="row">
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="label_new">
																<span class="new">new</span>
															</div>
															<div class="sale-off">
																<span class="sale-percent">-55%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/12.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/12bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
															</div>
															<div class="product-action">
																<div class="button-group">
																	<div class="product-button">
																		<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																	</div>
																	<div class="product-button-2">
																		<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																		<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																		<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal"><i class="fa fa-search-plus"></i></a>
																	</div>
																</div>
															</div>												
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="label_new">
																<span class="new">new</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/12bg.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/12.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Trid Palm</a></h5>
																<div class="price-box">
																	<span class="price">$92.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/2.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/2bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Trid Palm</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/2bg.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/2.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-20%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/5.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/3bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$92.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/6.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/4bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Trid Palm</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/5.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/4bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Ultra Consequad</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="label_new">
																<span class="new">new</span>
															</div>
															<div class="sale-off">
																<span class="sale-percent">-55%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/12.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/12bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Various Versions</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
															</div>
															<div class="product-action">
																<div class="button-group">
																	<div class="product-button">
																		<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																	</div>
																	<div class="product-button-2">
																		<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																		<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																		<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal"><i class="fa fa-search-plus"></i></a>
																	</div>
																</div>
															</div>												
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="label_new">
																<span class="new">new</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/12bg.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/12.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Trid Palm</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/2.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/2bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/2bg.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/2.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Ultra Consequad</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-20%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/5.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/3bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Trid Palm</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/10bg.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/13.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Various Versions</a></h5>
																<div class="price-box">
																	<span class="price">$99.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/13.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/10bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/11.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/4bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Ultra Consequad</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
													<!-- Start Single-Product -->
													<div class="col-md-3 col-sm-4 col-xs-12">
														<div class="single-product">
															<div class="sale-off">
																<span class="sale-percent">-25%</span>
															</div>
															<div class="product-img">
																<a href="#">
																	<img class="primary-img" src="public/img/product/mediam/10.jpg" alt="Product">
																	<img class="secondary-img" src="public/img/product/mediam/10bg.jpg" alt="Product">
																</a>
															</div>
															<div class="product-description">
																<h5><a href="#">Established Fact</a></h5>
																<div class="price-box">
																	<span class="price">$88.00</span>
																	<span class="old-price">$110.00</span>
																</div>
																<span class="rating">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-o"></i>
																</span>
																<div class="product-action">
																	<div class="button-group">
																		<div class="product-button">
																			<button><i class="fa fa-shopping-cart"></i> Add to Cart</button>
																		</div>
																		<div class="product-button-2">
																			<a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
																			<a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
																			<a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<!-- End Single-Product -->
												</div>
												<!-- Start Pagination Area -->
												<div class="pagination-area">
													<div class="row">
														<div class="col-xs-5">
															<div class="pagination">
																<ul>
																	<li class="active"><a href="#">1</a></li>
																	<li><a href="#">2</a></li>
																	<li><a href="#">></a></li>
																	<li><a href="#">>|</a></li>
																</ul>
															</div>
														</div>
														<div class="col-xs-7">
															<div class="product-result">
																<span>Showing 1 to 16 of 19 (2 Pages)</span>
															</div>
														</div>
													</div>
												</div>
												<!-- End Pagination Area -->
											</div>
											<!-- End Product = TV -->
										</div>
									</div>
									<!-- End Product -->
								</div>
							</div>
						</div>
						<!-- END PRODUCT-AREA -->
					</div>
				</div>
			</div>
			<!-- START BRAND-LOGO-AREA -->
			<div class="brand-logo-area carosel-navigation">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="area-title">
								<h3 class="title-group border-red gfont-1">Brand Logo</h3>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="active-brand-logo">
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/4.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/5.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/6.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/1.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/2.png" alt=""></a>
								</div>
							</div>
							<div class="col-md-2">
								<div class="single-brand-logo">
									<a href="#"><img src="public/img/brand/3.png" alt=""></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END BRAND-LOGO-AREA -->
		</section>